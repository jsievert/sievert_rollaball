﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //member variables
    public float speed;
    public Vector3 playerStart;
    public Text countText;
    public Text winText;
    public Text timerText;
    private Rigidbody rb;
    //mat refers to the player material
    private Material mat;
    private int count;
    public float timer;
    public bool timing;
    //these three booleans track what shade the player material should be.
    public bool blue;
    public bool green;
    public bool red;
    public string currentColor;
    public Button winBackground;

    // Start is called before the first frame update
    void Start()
    {
        //assigning variables to the member variables we declared at the beginning.
        rb = GetComponent<Rigidbody>();
        mat = GetComponent<Renderer>().material;
        playerStart = transform.position;
        count = 0;
        SetCountText();
        winText.text = "";
        timing = true;
        timer = 0;
        blue =false;
        green = false;
        red = false;
        currentColor = "black";
    }

    //this function makes sure there is no memory leak from changing the color of the player material.
    void OnDestroy()
    {
        Destroy(mat);
    }

    // Update is called once per frame, but FixedUpdate is not as frequent.
    void FixedUpdate()
    {
        //these floats take input from Unity as 1 0 or -1 to represent movement inputs.
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        //these inputs also take 1, 0, or -1 and we only care if they are not 0
        //right now we are creating many materials each frame. To improve this, we would want to use GetButtonDown
        //with that we would also need to create six input buttons, because GetButtonDown only returns true, not 1, 0, or -1 like GetAxes.
        if (Input.GetAxis("RedAssign") != 0)
        {
            //calls The function to change the red boolean variable
            RedAdjust(Input.GetAxis("RedAssign"));
        }
        if (Input.GetAxis("BlueAssign")!=0)
        {
            //calls the function to change the blue boolean variable
            BlueAdjust(Input.GetAxis("BlueAssign"));
        }
        if (Input.GetAxis("GreenAssign")!=0)
        {
            //calls the function to change the green boolean variable
            GreenAdjust(Input.GetAxis("GreenAssign"));
        }
        //creates a movement vector3 based on the inputs we got at the beginning of this function.
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        //uses that vector3 and our speed variable to change the velocity of the player's rigidbody.
        rb.AddForce(movement * speed);
        //while the game has not been finished
        if (timing)
        {
            //take the timer and add the time since the last frame.
            timer += Time.deltaTime;
            //calls a function that will set the timer text.
            SetTimerText();
        }
    }
    //This function is called when the player enters an object that has their collision set as a trigger.
    void OnTriggerEnter(Collider other)
    {
        //if the player collides with the respawn plane due to falling off...
        if (other.gameObject.CompareTag ("Respawn"))
        {
            //call the respawn function.
            respawn();
        }
        //each if statement checks if the collided pickup is a certain color, based off it's tag.
        if (other.gameObject.CompareTag ("YellowPickUp"))
        {
            //then we check whether the player's color matches it, making you able to pick up the object.
            if (currentColor =="yellow")
            {
                //if both object and player match colors, we disable the pickup object...
                other.gameObject.SetActive(false);
                //add one to the score count...
                count = count+1;
                //and call a function to update the Score Count Textbox.
                SetCountText();
            }
        }
        //all the else if statements in this function are just a repeat of the above function for each color.
        else if (other.gameObject.CompareTag ("RedPickUp"))
        {
            if (currentColor =="red")
            {
                other.gameObject.SetActive(false);
                count = count+1;
                SetCountText();
            }
        }
        else if (other.gameObject.CompareTag ("BlackPickUp"))
        {
            if (currentColor =="black")
            {
                other.gameObject.SetActive(false);
                count = count+1;
                SetCountText();
            }
        }
        else if (other.gameObject.CompareTag ("BluePickUp"))
        {
            if (currentColor =="blue")
            {
                other.gameObject.SetActive(false);
                count = count+1;
                SetCountText();
            }
        }
        else if (other.gameObject.CompareTag ("GreenPickUp"))
        {
            if (currentColor =="green")
            {
                other.gameObject.SetActive(false);
                count = count+1;
                SetCountText();
            }
        }
        else if (other.gameObject.CompareTag ("CyanPickUp"))
        {
            if (currentColor =="cyan")
            {
                other.gameObject.SetActive(false);
                count = count+1;
                SetCountText();
            }
        }
        else if (other.gameObject.CompareTag ("MagentaPickUp"))
        {
            if (currentColor =="magenta")
            {
                other.gameObject.SetActive(false);
                count = count+1;
                SetCountText();
            }
        }
        else if (other.gameObject.CompareTag ("WhitePickUp"))
        {
            if (currentColor =="white")
            {
                other.gameObject.SetActive(false);
                count = count+1;
                SetCountText();
            }
        }
    }
    //This function will update the Score Count Text when called.
    void SetCountText()
    {
        //We need to make the count variable a string, so we call ToString() on it in order to properly format it.
        countText.text = "Score: "+count.ToString();
        //if all pickups have been gathered...
        if (count >= 18)
        {
            //Set win text to "You Win!"
            winText.text = "You Win!";
            winBackground.gameObject.SetActive(true);
            //turn off timing, so that the timer will no longer update, giving the player their time upon completion.
            timing = false;
        }
    }
    //This function will update the Timer Text when called.
    void SetTimerText()
    {
        //in order to make sure we have no errors due to the float occasionally being less than 4 long we call this if statement.
        if(timer.ToString().Length>4)
        {
            //when the timer gets larger than 100 we want a longer substring so we can still display tenths of a second.
            if(timer<=100)
            {
                //when the timer variable is longer than length of 4, we take only the first four characters, so that it has a nice format for the UI.
                timerText.text = "Timer: "+timer.ToString().Substring(0,4);
                
            }
            else
            {
                timerText.text = "Timer: "+timer.ToString().Substring(0,5);
            }
        }
        //when the time is already four characters or less...
        else
        {
            //we only need to make it a string, and don't have to trim it to a shorter length.
             timerText.text = "Timer: "+timer.ToString();
        }
    }
    //This will set the boolean variable to true or false, and then call a function to update the player material color.
    void RedAdjust(float adjuster)
    {
        //adjuster was our player input. When we recieve positive input...
        if (adjuster==1)
        {
            //we set the red variable to true, which will be akin to setting the Red RGB value to 255.
            red = true;
        }
        //when we recieve negative input...
        if (adjuster == -1)
        {
            //we set the red variable to false, which will be akin to setting the Red RGB value to 0.
            red = false;
        }
        //ColorAdjust() updates the player material.
        ColorAdjust();
    }
    //BlueAdjust works exactly like RedAdjust() but focusing on the blue RGB values.
    void BlueAdjust(float adjuster)
    {
        if (adjuster==1)
        {
            blue = true;
        }
        if (adjuster == -1)
        {
            blue = false;
        }
        ColorAdjust();
    }
    //GreenAdjust works exactly like RedAdjust() and BlueAdjust(), but focusing on the green RGB values.
    void GreenAdjust(float adjuster)
    {
        if (adjuster==1)
        {
            green = true;
        }
        if (adjuster == -1)
        {
            green = false;
        }
        ColorAdjust();
    }
    //This function goes through a series of if checks to determine what color the player material should be set to.
    //true values consider that color to be set to 255 in the RGB variable.
    //false values consider that color to be set to 0 in the RGB variable.
    //Since I am using SetColor and the preset colors, this is not exact, but I have selected the colors closest to the above assumptions.
    void ColorAdjust()
    {
        //if blue is true..
        if (blue)
        {
            //red is true..
            if (red)
            {
                //and green is true.
                if (green)
                {
                    //make the material white.
                    mat.SetColor("_Color",Color.white);
                    //save an easily accessible string with the color
                    currentColor="white";
                }
                //green is false
                else
                {
                    //make the material magenta
                    mat.SetColor("_Color",Color.magenta);
                    //save easily accesible string w/ the color.
                    currentColor="magenta";
                }
            }
            //red is false...
            //and green is true...
            else if (green)
            {
                //set color to cyan
                mat.SetColor("_Color",Color.cyan);
                //save easily accesible string w/ the color
                currentColor="cyan";
            }
            //green is false...
            else
            {
                //set color to blue.
                mat.SetColor("_Color", Color.blue);
                //save easily accesible string w/ the color
                currentColor="blue";
            }
        }
        //blue is false...
        //red is true...
        else if (red)
        {
            //green is true...
            if (green)
            {
                //set color to yellow
                mat.SetColor("_Color",Color.yellow);
                //save easily accesible string w/ the color
                currentColor="yellow";
            }
            //green is false...
            else
            {
                //set color to red
                mat.SetColor("_Color",Color.red);
                //save easily accesible string w/ the color
                currentColor="red";
            }
        }
        //blue and red are false...
        //green is true...
        else if (green)
        {
            //set color to green
            mat.SetColor("_Color",Color.green);
            //save easily accesible string w/ the color
            currentColor="green";
        }
        //blue, red, and green are false...
        else
        {
            //set color to black
            mat.SetColor("_Color",Color.black);
            //save easily accesible string w/ the color
            currentColor="black";
        }
    }
    //this function respawns the player
    void respawn()
    {
        //will set the player back to the spawnpoint
        transform.position = playerStart;
    }
}