﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //member variables
    public GameObject player;
    private Vector3 offset;
    

    // Start is called before the first frame update
    void Start()
    {
        //we save the difference between the player's starting position and the camera's starting position so that we can keep that constant.
        offset = transform.position - player.transform.position;
    }

    // Update is called once per frame, meanwhile LateUpdate is called at the very END of the frame. 
    //This allows us to make sure the ball will move first every time, so there is no camera jitter.
    void LateUpdate()
    {
        //after the player has moved in the other update functions, 
        //we then adjust the camera by referencing the player's position and just adding the offset constant we got at the beginning.
        transform.position = player.transform.position + offset;
    }
}
