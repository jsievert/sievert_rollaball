﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        //this script rotates any object it is attached to. It will rotate the slowest in the X direction and fastest in the Z direction.
        //We multiple it by Time.deltaTime so that it will move at a steady pace, regardless of frame lag.
        transform.Rotate(new Vector3(15,30,45)*Time.deltaTime);
    }
}
